﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculadoraNomina
{
    public partial class Form1 : Form
    {
        Double SueldoMensual,DiasNomina,SueldoDiario,DiasVacaciones;//Ingreso de datos
        Double  Aguinaldo,SueldoD,Vacaciones,Infonavit,Rcv,Despensa,Imss,Neto;

        private void lblSd_Click(object sender, EventArgs e)
        {

        }

        public Form1()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            SueldoMensual = Double.Parse(txtSueldo.Text);
            DiasNomina = Double.Parse(txtNomina.Text);
            DiasVacaciones = Double.Parse(txtDiaV.Text);


            SueldoD = SueldoMensual / DiasNomina;
            //lblSd.Text ="$"+SueldoD.ToString();

            Aguinaldo = SueldoMensual / DiasNomina * 15;
            lblA.Text = "$"+ Aguinaldo.ToString();

            Vacaciones = SueldoD * DiasVacaciones * .25;
            lblV1.Text = "$"+ Vacaciones.ToString();

            Rcv = (SueldoMensual) * (0.01125) * 2;
            lblR.Text = "$"+ Rcv.ToString();

            Despensa = SueldoMensual * 0.01;
            lblDes.Text = "$" +Despensa.ToString();

            Infonavit = SueldoMensual * 0.05;
            lblInfo.Text = "$" + Infonavit.ToString();

            Imss = SueldoMensual / DiasNomina * 0.0025 + SueldoMensual / DiasNomina * 0.00375 + SueldoMensual / DiasNomina * 0.00625;
            lblIm.Text = "$" + Imss.ToString();

            Neto = SueldoMensual - Rcv - Despensa - Infonavit - Imss;
            label2.Text = "$" + Neto.ToString();

        }

        private void lblA_Click(object sender, EventArgs e)
        {

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {   //Cuadros de texto
            txtNomina.Clear();
            txtSueldo.Clear();
            txtDiaV.Clear();

            //Etiquetas
            lblA.Text = "";
            lblDv.Text = "";
            lblDes.Text = "";
            lblR.Text = "";
            lblIm.Text="";
            label2.Text = "";
            lblV1.Text = "";
            lblInfo.Text = "";

        }

        private void lblNomina_Click(object sender, EventArgs e)
        {

        }

        private void lblVacaciones_Click(object sender, EventArgs e)
        {
        }

        private void txtSueldo_TextChanged(object sender, EventArgs e)
        {
          
        }
    }
}
