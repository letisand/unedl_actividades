﻿namespace CalculadoraNomina
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSueldo = new System.Windows.Forms.Label();
            this.lblNomina = new System.Windows.Forms.Label();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.txtNomina = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDes = new System.Windows.Forms.Label();
            this.lblR = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblV1 = new System.Windows.Forms.Label();
            this.lblIm = new System.Windows.Forms.Label();
            this.lblA = new System.Windows.Forms.Label();
            this.lblDespensa = new System.Windows.Forms.Label();
            this.lblRcv = new System.Windows.Forms.Label();
            this.lblInfonavit = new System.Windows.Forms.Label();
            this.lblImss = new System.Windows.Forms.Label();
            this.lblAguinaldo = new System.Windows.Forms.Label();
            this.lblVacaciones = new System.Windows.Forms.Label();
            this.lblDv = new System.Windows.Forms.Label();
            this.txtDiaV = new System.Windows.Forms.TextBox();
            this.lblSueldoD = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSueldo
            // 
            this.lblSueldo.AutoSize = true;
            this.lblSueldo.Location = new System.Drawing.Point(21, 25);
            this.lblSueldo.Name = "lblSueldo";
            this.lblSueldo.Size = new System.Drawing.Size(83, 13);
            this.lblSueldo.TabIndex = 0;
            this.lblSueldo.Text = "Sueldo Mensual";
            // 
            // lblNomina
            // 
            this.lblNomina.AutoSize = true;
            this.lblNomina.Location = new System.Drawing.Point(25, 67);
            this.lblNomina.Name = "lblNomina";
            this.lblNomina.Size = new System.Drawing.Size(69, 13);
            this.lblNomina.TabIndex = 1;
            this.lblNomina.Text = "Días Nómina";
            this.lblNomina.Click += new System.EventHandler(this.lblNomina_Click);
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(40, 266);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(83, 25);
            this.btnGenerar.TabIndex = 3;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(173, 266);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(83, 25);
            this.btnLimpiar.TabIndex = 4;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // txtSueldo
            // 
            this.txtSueldo.Location = new System.Drawing.Point(161, 18);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(100, 20);
            this.txtSueldo.TabIndex = 5;
            this.txtSueldo.TextChanged += new System.EventHandler(this.txtSueldo_TextChanged);
            // 
            // txtNomina
            // 
            this.txtNomina.Location = new System.Drawing.Point(161, 64);
            this.txtNomina.Name = "txtNomina";
            this.txtNomina.Size = new System.Drawing.Size(100, 20);
            this.txtNomina.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblDes);
            this.groupBox1.Controls.Add(this.lblR);
            this.groupBox1.Controls.Add(this.lblInfo);
            this.groupBox1.Controls.Add(this.lblV1);
            this.groupBox1.Controls.Add(this.lblIm);
            this.groupBox1.Controls.Add(this.lblA);
            this.groupBox1.Controls.Add(this.lblDespensa);
            this.groupBox1.Controls.Add(this.lblRcv);
            this.groupBox1.Controls.Add(this.lblInfonavit);
            this.groupBox1.Controls.Add(this.lblImss);
            this.groupBox1.Controls.Add(this.lblAguinaldo);
            this.groupBox1.Controls.Add(this.lblVacaciones);
            this.groupBox1.Location = new System.Drawing.Point(349, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 148);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deducciones";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lblDes
            // 
            this.lblDes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDes.Location = new System.Drawing.Point(298, 73);
            this.lblDes.Name = "lblDes";
            this.lblDes.Size = new System.Drawing.Size(69, 21);
            this.lblDes.TabIndex = 15;
            // 
            // lblR
            // 
            this.lblR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblR.Location = new System.Drawing.Point(298, 24);
            this.lblR.Name = "lblR";
            this.lblR.Size = new System.Drawing.Size(69, 24);
            this.lblR.TabIndex = 12;
            // 
            // lblInfo
            // 
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Location = new System.Drawing.Point(298, 106);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(69, 24);
            this.lblInfo.TabIndex = 13;
            // 
            // lblV1
            // 
            this.lblV1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblV1.Location = new System.Drawing.Point(89, 25);
            this.lblV1.Name = "lblV1";
            this.lblV1.Size = new System.Drawing.Size(69, 23);
            this.lblV1.TabIndex = 9;
            this.lblV1.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // lblIm
            // 
            this.lblIm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblIm.Location = new System.Drawing.Point(89, 107);
            this.lblIm.Name = "lblIm";
            this.lblIm.Size = new System.Drawing.Size(69, 23);
            this.lblIm.TabIndex = 10;
            // 
            // lblA
            // 
            this.lblA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblA.Location = new System.Drawing.Point(89, 71);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(69, 23);
            this.lblA.TabIndex = 11;
            this.lblA.Click += new System.EventHandler(this.lblA_Click);
            // 
            // lblDespensa
            // 
            this.lblDespensa.AutoSize = true;
            this.lblDespensa.Location = new System.Drawing.Point(187, 71);
            this.lblDespensa.Name = "lblDespensa";
            this.lblDespensa.Size = new System.Drawing.Size(105, 13);
            this.lblDespensa.TabIndex = 6;
            this.lblDespensa.Text = "Vales de Despensa :";
            // 
            // lblRcv
            // 
            this.lblRcv.AutoSize = true;
            this.lblRcv.Location = new System.Drawing.Point(232, 33);
            this.lblRcv.Name = "lblRcv";
            this.lblRcv.Size = new System.Drawing.Size(32, 13);
            this.lblRcv.TabIndex = 4;
            this.lblRcv.Text = "RCV:";
            // 
            // lblInfonavit
            // 
            this.lblInfonavit.AutoSize = true;
            this.lblInfonavit.Location = new System.Drawing.Point(213, 108);
            this.lblInfonavit.Name = "lblInfonavit";
            this.lblInfonavit.Size = new System.Drawing.Size(51, 13);
            this.lblInfonavit.TabIndex = 3;
            this.lblInfonavit.Text = "Infonavit:";
            // 
            // lblImss
            // 
            this.lblImss.AutoSize = true;
            this.lblImss.Location = new System.Drawing.Point(14, 108);
            this.lblImss.Name = "lblImss";
            this.lblImss.Size = new System.Drawing.Size(36, 13);
            this.lblImss.TabIndex = 2;
            this.lblImss.Text = "IMSS:";
            // 
            // lblAguinaldo
            // 
            this.lblAguinaldo.AutoSize = true;
            this.lblAguinaldo.Location = new System.Drawing.Point(14, 71);
            this.lblAguinaldo.Name = "lblAguinaldo";
            this.lblAguinaldo.Size = new System.Drawing.Size(57, 13);
            this.lblAguinaldo.TabIndex = 1;
            this.lblAguinaldo.Text = "Aguinaldo:";
            // 
            // lblVacaciones
            // 
            this.lblVacaciones.AutoSize = true;
            this.lblVacaciones.Location = new System.Drawing.Point(14, 33);
            this.lblVacaciones.Name = "lblVacaciones";
            this.lblVacaciones.Size = new System.Drawing.Size(69, 13);
            this.lblVacaciones.TabIndex = 0;
            this.lblVacaciones.Text = "Vacaciones :";
            this.lblVacaciones.Click += new System.EventHandler(this.lblVacaciones_Click);
            // 
            // lblDv
            // 
            this.lblDv.AutoSize = true;
            this.lblDv.Location = new System.Drawing.Point(21, 111);
            this.lblDv.Name = "lblDv";
            this.lblDv.Size = new System.Drawing.Size(102, 13);
            this.lblDv.TabIndex = 9;
            this.lblDv.Text = "Dias de Vacaciones";
            // 
            // txtDiaV
            // 
            this.txtDiaV.Location = new System.Drawing.Point(161, 104);
            this.txtDiaV.Name = "txtDiaV";
            this.txtDiaV.Size = new System.Drawing.Size(100, 20);
            this.txtDiaV.TabIndex = 10;
            // 
            // lblSueldoD
            // 
            this.lblSueldoD.AutoSize = true;
            this.lblSueldoD.Location = new System.Drawing.Point(50, 192);
            this.lblSueldoD.Name = "lblSueldoD";
            this.lblSueldoD.Size = new System.Drawing.Size(0, 13);
            this.lblSueldoD.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblSueldo);
            this.groupBox2.Controls.Add(this.lblNomina);
            this.groupBox2.Controls.Add(this.txtDiaV);
            this.groupBox2.Controls.Add(this.lblDv);
            this.groupBox2.Controls.Add(this.txtSueldo);
            this.groupBox2.Controls.Add(this.txtNomina);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(332, 148);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(349, 232);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(402, 59);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sueldo Neto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sueldo Total :";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Location = new System.Drawing.Point(98, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 25);
            this.label2.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblSueldoD);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnGenerar);
            this.Name = "Form1";
            this.Text = "Calculadora_de_Nomina";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSueldo;
        private System.Windows.Forms.Label lblNomina;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.TextBox txtNomina;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblInfonavit;
        private System.Windows.Forms.Label lblImss;
        private System.Windows.Forms.Label lblAguinaldo;
        private System.Windows.Forms.Label lblVacaciones;
        private System.Windows.Forms.Label lblRcv;
        private System.Windows.Forms.Label lblDespensa;
        private System.Windows.Forms.Label lblDes;
        private System.Windows.Forms.Label lblR;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label lblV1;
        private System.Windows.Forms.Label lblIm;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblDv;
        private System.Windows.Forms.TextBox txtDiaV;
        private System.Windows.Forms.Label lblSueldoD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

