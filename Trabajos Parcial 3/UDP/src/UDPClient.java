
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class UDPClient {
    
     private DatagramSocket socket;
    private InetAddress address;
    private byte[]buf;

    public UDPClient() {
       try {
           try {
               socket = new DatagramSocket();
           } catch (SocketException ex) {
               Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
           }
           address = InetAddress.getByName("localhost");
       } catch (UnknownHostException ex) {
           Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
     
      public String sendEcho(String msg) {
       try {
           buf = msg.getBytes();
           DatagramPacket packet
                   = new DatagramPacket(buf, buf.length, address, 4445);
           socket.send(packet);
           packet = new DatagramPacket(buf, buf.length);
           socket.receive(packet);
           String received = new String(
                   packet.getData(), 0, packet.getLength());
           return received;
       } catch (IOException ex) {
           Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
       }
       return null;
    }
      
  public void close() {
        socket.close();
    }
    
    
    
}
