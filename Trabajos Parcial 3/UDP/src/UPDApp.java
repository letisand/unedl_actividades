
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lety
 */
public class UPDApp extends Thread{
    
private DatagramSocket socket;
    private boolean running;
    private byte[] buf = new byte[256];
 
    
      public UPDApp() throws SocketException {
        socket = new DatagramSocket(4445);
    }
      
@Override
      public void run() {
        running = true;
 
        while (running) {
            DatagramPacket packet
                    = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (java.io.IOException ex) {
                Logger.getLogger(UPDApp.class.getName()).log(Level.SEVERE, null, ex);
            }
            InetAddress address = packet.getAddress();
            int port = packet.getPort();
            packet = new DatagramPacket(buf, buf.length, address, port);
            String received
                    = new String(packet.getData(), 0, packet.getLength());
            if (received.equals("end")) {
                running = false;
                continue;
            }
            try {
                socket.send(packet);
            } catch (java.io.IOException ex) {
                Logger.getLogger(UPDApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        socket.close();
    }  
}