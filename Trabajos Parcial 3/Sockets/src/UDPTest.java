package udpapp;

import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;*/

public class UDPTest {
    
 private UDPClient client;
   
 
  public void setup(){
     try {
         new UDPApp().start();
         client = new UDPClient();
     } catch (SocketException ex) {
         Logger.getLogger(UDPTest.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
  
  //error al importar junit, que no existen los paquetes que estan comentados, lineas 7 y 8
  public void whenCanSendAndReceivePacket_thenCorrect() {
        String echo = client.sendEcho("hello server");
        assertEquals("hello server", echo);
        echo = client.sendEcho("server is working");
        assertFalse(echo.equals("hello server"));
    }
  
  
  public void tearDown() {
        client.sendEcho("end");
        client.close();
    }
    public static void main(String[] args) {
       
    }
    
}
