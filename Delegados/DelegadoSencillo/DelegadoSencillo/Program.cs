﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegadoSencillo
{
    public delegate void Delegado(string m);


    class Program
    {
        static void Main(string[] args)
        {
            Delegado delegado =new Delegado (CRadio.MetodoRadio);

            delegado("Hola a todos");

            delegado = new Delegado(CPastel.MostrarPastel);

            delegado("Feliz Cumpleaños");


        }

        
    }
}
