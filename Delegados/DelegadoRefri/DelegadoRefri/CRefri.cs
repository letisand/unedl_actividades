﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegadoRefri
{
    class CRefri
    {
        delegate void DReservasBajas(int Kilos);
        delegate void DDescongelado(int Grados);


        class CRefri
        {
            private int kilosA = 0;
            private int grados = 0;

            //variables para invocar
            private DReservasBajas delReservas;
            private DDescongelado delDescongelado;

            public CRefri(int Kilos, int Grados)
            {
                kilosA = Kilos;
                grados = Grados;
            }

            public void AdicionMetodoReservas(DReservasBajas Metodo)
            {
                delReservas += Metodo;
            }

            public void EliminaMetodoReservas(DReservasBajas Metodo)
            {
                delReservas -= Metodo;
            }

            public void AdcionMetodoDescongelado(DDescongelado Metodo)
            {
                delDescongelado += Metodo;

            }

            public int Kilos { get { return kilosA; } set { kilosA = value; } }
            public int Grados { get { return grados; } set { grados = value; } }

            public void Trabajar(int Consumo)
            {
                //Act. Kilos
                kilosA -= Consumo;

                //Subir temperatura
                grados += 1;

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("{0} kilos, {1} grados.", kilosA, grados);

                if (kilosA < 10)
                {
                    delReservas(kilosA);
                }

                if (grados > 0)
                {
                    delDescongelado(grados);
                }
            }
        }
}
