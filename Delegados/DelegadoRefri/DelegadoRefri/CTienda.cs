﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegadoRefri
{
    class CTienda
    {
        public static void MandaViveres(int kilos)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Estoy en la tienda, se enviarán sus viveres");
            Console.WriteLine("Seran {0} kilos", kilos);
        }
    }
}
