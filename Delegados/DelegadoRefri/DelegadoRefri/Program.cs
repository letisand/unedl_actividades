﻿using System;

namespace DelegadoRefri
{
    class Program
    {
       os handlers

            Refri.AdicionaMetodoReservas(new DelegadoRefri.DReservasBajas(InformeKilos));
            Refri.AdicionaMetodoReservas(new DelegadoRefri.Descongelado(InformeGrados));

            //el refri trabaja
static void Main(string[] args)
        {
            //Creacion del refri
            CRefri miRefri = new CRefri(70, -20);
            Random rnd = new Random();

            //Multicasting
            DReservasBajas kilos1 = new DReservasBajas(InformeKilos);
            DReservasBajas kilos2 = new DReservasBajas(CTienda.MandaViveres);

            DDescongelado desc1 = new DDescongelado(InformeGrados);

            //Adicion de lo handlers
            miRefri.AdicionMetodoReservas(kilos1);
            miRefri.AdicionMetodoReservas(kilos2);
            miRefri.AdcionMetodoDescongelado(desc1);

            //Refri trabajando
            while (miRefri.Kilos > 0)
            {
                miRefri.Trabajar(rnd.Next(1, 5));
            }

            miRefri.EliminaMetodoReservas(kilos2);

  
            miRefri.Kilos = 50;
            miRefri.Grados = -15;

            while (miRefri.Kilos > 0)
            {
                miRefri.Trabajar(rnd.Next(1, 5));
            }
        }

        public static void InformeKilos(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Casi vacio de comida.");
            Console.WriteLine("Quedan {0} kilos.", pKilos);
        }

        public static void InformeGrados(int pGrados)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Refir descongelandose.");
            Console.WriteLine("La temperatura son {0} grados.", pGrados);
        }

    }
}
