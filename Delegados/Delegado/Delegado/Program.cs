﻿using System;

delegate int CambioNumero(int n);
namespace Delegado
{
    class Program
    {
        static int num = 10;

        public static int AddNum(int p)
        {
            num += p;
            return num;
        }
        public static int MultNum(int q)
        {
            num *= q;
            return num;
        }
        public static int getNum()
        {
            return num;
        }
        static void Main(string[] args)
        {
            //delegando instancias
            CambioNumero nc1 = new CambioNumero(AddNum);
            CambioNumero nc2 = new CambioNumero(MultNum);

            nc1(25);
            Console.WriteLine("Valor del num: {0}", getNum());
            nc2(5);
            Console.WriteLine("Valor del num: {0}", getNum());
            Console.ReadKey();
        }
    }
}
