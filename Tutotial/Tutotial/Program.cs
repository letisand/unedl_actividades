﻿using System;

delegate int NumberChanger(int n);
namespace Tutotial

    

{
    class TestTutotial
{
    static int numero= 10;

    public static int AddNum(int p)
    {
        numero += p;
        return numero;
    }
    public static int MultNum(int q)
    {
        numero *= q;
        return numero;
    }
    public static int getNum()
    {
        return numero;
    }
    static void Main(string[] args)
    {
        //create delegate instances
        NumberChanger nc1 = new NumberChanger(AddNum);
        NumberChanger nc2 = new NumberChanger(MultNum);

        //calling the methods using the delegate objects
        nc1(25);
        Console.WriteLine("Value of Num: {0}", getNum());
        nc2(5);
        Console.WriteLine("Value of Num: {0}", getNum());
        Console.ReadKey();
    }
}
}
