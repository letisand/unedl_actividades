﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExamenParcial2
{
    class Program
    {
        private static Semaphore trabajadoresconstruccion;
        private static int _padding;
        private static int t = 0;
        private static int r = 0;
        private static int o = 0;
        private static int j;

        private static void SemaforoTrabajadores(object trabajador)
        {
            Console.WriteLine("Constructor");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Constructor{0} esperando turno...",trabajador);
            trabajadoresconstruccion.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Constructor {0} obtiene turno...",trabajador);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("constructor {0} termina turno...",trabajador);

            Console.WriteLine("trabajador {0} libera su lugar {1}",
                trabajador, trabajadoresconstruccion.Release());
        }



        public static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos trabajadores son?");
            t = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos pueden trabajar?");
            r = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            String texto, texto2, texto3;
            int casas, habitaciones, baños;

            Console.WriteLine("Ingresa la cantidad de casas a fabricar ");
            casas = int.Parse(texto = Console.ReadLine());
            Console.WriteLine("Ingresa cantidad de habitaciones por casa: ");
            habitaciones = int.Parse(texto2 = Console.ReadLine());
            Console.WriteLine("Ingresa cantidad de baños por casa: ");
            baños = int.Parse(texto3 = Console.ReadLine());

            var tasks = new Task[casas];
            var rnd = new Random();

            var source1 = new CancellationTokenSource();
            var token1 = source1.Token;


            for (int i = 0; i < habitaciones; i++)
            {
                for (int j = 0; j < casas; j++)
                    tasks[j] = Task.Run(() => Thread.Sleep(rnd.Next(500, 3000)));
                tasks[j] = Task.Run(() =>
                {
                    Thread.Sleep(rnd.Next(2000));
                    if (token1.IsCancellationRequested)
                        token1.ThrowIfCancellationRequested();
                    Thread.Sleep(rnd.Next(500));
                }, token1);

                try
                {
                    int index = Task.WaitAny(tasks);
                    Console.WriteLine("Tarea #{0} termino primero.\n", tasks[index].Id);
                    Task.WaitAll(tasks);
                    Console.WriteLine("\nTarea #{0} termino primero.", index + 1);
                    Console.WriteLine("Estatus de las tareas: ");
                    foreach (var t in tasks)
                        Console.WriteLine("   Tarea #{0}: {1}", t.Id, t.Status);
                    for (int k = 0; k < casas; k++)
                        Console.WriteLine("   Tarea #{0}: {1}", k + 1, tasks[k].Status);
                }
                catch (AggregateException)
                {
                    Console.WriteLine("ocurrió un error .");
                }

                trabajadoresconstruccion = new Semaphore(0, r);
                Console.WriteLine("iniciar construcción");
                for (int o = 1; o <= t; o++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(SemaforoTrabajadores));
                    if (o % 2 == 0)
                    {
                        t.Priority = ThreadPriority.AboveNormal;
                    }
                    else
                    {
                        t.Priority = ThreadPriority.BelowNormal;
                    }
                    t.Start(o);
                }
                Thread.Sleep(500);
                Console.WriteLine("Se libera un turno");
                trabajadoresconstruccion.Release(r);

                Thread.Sleep(o);
                Console.WriteLine(" terminó la construcción");



               }

            }

        }
     }
 


    