﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Unedl
{
    public partial class Calculadora_Unedl : Form
    {
        public Calculadora_Unedl()
        {
            InitializeComponent();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {
           
        }

        private void button8_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "8";
        }

        private void btnUno_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "1";
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "2";
        }

        private void btnTres_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "3";
        }

        private void btncuatro_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "4";
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "5";
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "6";
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "7";
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "9";
        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "0";
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text.Substring(0,txtResultado.Text.Length -1);
        }
    }
}
