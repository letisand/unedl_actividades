﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CarreraAutos
{
    class Program
    {
        private static int j;

        public static void Main()
        {
            String texto, texto2;
            int autos, vueltas;

            Console.WriteLine("Ingresa número de autos en carrera: ");
            autos = int.Parse(texto = Console.ReadLine());
            Console.WriteLine("Ingresa número de vueltas de la carrera: ");
            vueltas = int.Parse(texto2 = Console.ReadLine());

            var tasks = new Task[autos];
            var rnd = new Random();

            var source1 = new CancellationTokenSource();
            var token1 = source1.Token;
            //source1.Cancel();

            for (int i = 0; i < vueltas; i++)
            {
                for (int j = 0; j < autos; j++)
                    tasks[j] = Task.Run(() => Thread.Sleep(rnd.Next(500, 3000)));
                tasks[j] = Task.Run(() =>
                {
                    Thread.Sleep(rnd.Next(2000));
                    if (token1.IsCancellationRequested)
                        token1.ThrowIfCancellationRequested();
                    Thread.Sleep(rnd.Next(500));
                }, token1);

                try
                {
                    int index = Task.WaitAny(tasks);
                    Console.WriteLine("Tarea #{0} termino primero.\n", tasks[index].Id);
                    Task.WaitAll(tasks);
                    Console.WriteLine("\nTarea #{0} termino primero.", index + 1);
                    Console.WriteLine("Estatus de las tareas: ");
                    foreach (var t in tasks)
                        Console.WriteLine("   Tarea #{0}: {1}", t.Id, t.Status);
                    for (int k = 0; k < autos; k++)
                        Console.WriteLine("   Tarea #{0}: {1}", k + 1, tasks[k].Status);
                }
                catch (AggregateException)
                {
                    Console.WriteLine("ocurrió un error .");
                }
            }
        }
    }
}